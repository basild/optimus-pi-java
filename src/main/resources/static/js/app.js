const client = new StompJs.Client({
  brokerURL: 'ws://' + window.location.host + '/robot',
  connectHeaders: {
    login: 'user',
    passcode: 'password',
  },
  debug: function (str) {
    console.log("Debug: " + str);
  },
  reconnectDelay: 5000,
  heartbeatIncoming: 4000,
  heartbeatOutgoing: 4000,
});

client.onConnect = function (frame) {
  // Do something, all subscribes must be done is this callback
  // This is needed because this will be executed after a (re)connect
  client.subscribe("/topic/sensor/line", (message) => {
    console.log("Line Sensor Update Received: " + message.body);
    updateLineSensorIndicator(JSON.parse(message.body));
  });
  client.subscribe("/topic/sensor/distance", (message) => {
    console.log("Distance Sensor Update Received: " + message.body);
  });
};

client.onStompError = function (frame) {
  // Will be invoked in case of error encountered at Broker
  // Bad login/passcode typically will cause an error
  // Complaint brokers will set `message` header with a brief message. Body may contain details.
  // Compliant brokers will terminate the connection after any error
  console.log('Broker reported error: ' + frame.headers['message']);
  console.log('Additional details: ' + frame.body);
};

client.activate();

class ControlPosition {
  name = "undefined"
  #x = 0;
  #y = 0;
  changed = false

  constructor(name, x, y) {
    this.x = x || 0;
    this.y = y || 0;
    this.name = name
    this.changed = true
  }

  xPos() {
    return this.x;
  }

  yPos() {
    return this.y;
  }

  /**
   * @param {int} xVal
   */
  set xOnly(xVal){
    this.updatePos(xVal, this.y);
  }

  /**
   * @param {int} yVal
   */
  set yOnly(yVal){
    this.updatePos(this.x, yVal);
  }

  updatePos(x, y){
    if(this.x === x && this.y === y){
      return;
    }
    this.changed = true;
    this.x = x;
    this.y = y;
  }

  changeRead(){
    this.changed = false;
  }

  hasChanged(){
    return this.changed;
  }
  
}

var cameraControl = new ControlPosition("camera", 0, 0);
var moveControl = new ControlPosition("move", 0, 0);

var joyCamera = new JoyStick('joy-camera', { "title": "joy-camera-canvas", "autoReturnToCenter": false }, (stickStatus) => {
  cameraControl.updatePos(stickStatus.x, stickStatus.y);
  if(cameraControl.hasChanged()){
    console.log(`Camera Stick Changed: X: ${stickStatus.x} - Y: ${stickStatus.y}`);
  }
});
var joyMove = new JoyStick('joy-move', { "title": "joy-move-canvas" }, (stickStatus) => {
  moveControl.updatePos(stickStatus.x, stickStatus.y);
  if(moveControl.hasChanged()){
    console.log(`Move Stick Changed: X: ${stickStatus.x} - Y: ${stickStatus.y}`);
  }
});

var keyMap = {};
function keyboardEvents(event) {
  var key = event.key;
  var code = event.code;
  
  keyMap[code] = event.type == 'keydown';

  console.log(`${event.type} - ${key} - Key code value: ${code}`);
  if(keyMap['ArrowUp']){
    console.log('Full Forward');
    moveControl.yOnly = 100
  } else if(keyMap['ArrowDown']){
    console.log('Full  Backward');
    moveControl.yOnly = -100
  } else {
    console.log('No vertical move')
    moveControl.yOnly = 0
  }

  if(keyMap['ArrowLeft']){
    console.log('Move Left');
    moveControl.xOnly = -100
  } else if(keyMap['ArrowRight']){
    console.log('Move Right');
    moveControl.xOnly = 100
  } else {
    console.log('No horizontal move');
    moveControl.xOnly = 0
  }

  if(keyMap['KeyR']){
    console.log('Resetting Camera Position');
    joyCamera.ResetPosition();
  }
}

document.addEventListener("keydown", keyboardEvents, false);
document.addEventListener("keyup", keyboardEvents, false);

function sendUpdates(){
  if(cameraControl.hasChanged()){
    console.log("Sending updated camera position to server");
    client.publish({ destination: '/app/control/camera', body: '{"name": "Camera", "x": ' + cameraControl.xPos() + ', "y": ' + cameraControl.yPos() + '}' });
    cameraControl.changeRead();
  }

  if(moveControl.hasChanged()){
    console.log("Sending updated move position to server");
    client.publish({ destination: '/app/control/move', body: '{"name": "Move", "x": ' + moveControl.xPos() + ', "y": ' + moveControl.yPos() + '}' });
    moveControl.changeRead();
  }
}

function updateLineSensorIndicator(lineSensorUpdate){
  console.log("Received line sensor update at: " + lineSensorUpdate.updatedOn);
  lineSensorUpdate.lineSensors.forEach(lineSensor => {
    var sensorIndicator = document.getElementById("line-sensor-" + lineSensor.name);
    // TODO: Can consider removing the remaining extra sensors perhaps as well
    sensorIndicator.classList.remove("line-sensor-off")
    sensorIndicator.classList.remove("line-sensor-on")
    if(lineSensor.state === "ON_LINE"){
      sensorIndicator.classList.add("line-sensor-on")
    } else {
      sensorIndicator.classList.add("line-sensor-off")
    }
  });
}

function updateDistanceSensorIndicator(){
}

setInterval(sendUpdates, 1000 / 20);

