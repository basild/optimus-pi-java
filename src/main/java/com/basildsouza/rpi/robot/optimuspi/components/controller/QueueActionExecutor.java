package com.basildsouza.rpi.robot.optimuspi.components.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@Slf4j
public class QueueActionExecutor {
    private final BlockingQueue<Runnable> actionQueue = new LinkedBlockingQueue<>();
    private final boolean executeOnlyLatest;

    public QueueActionExecutor(String name, boolean executeOnlyLatest) {
        this.executeOnlyLatest = executeOnlyLatest;
        Thread actionThread = new Thread(this::processActionQueue, name + "-Thread");
        actionThread.setDaemon(true);
        actionThread.start();
    }

    public void registerAction(Runnable action){
        actionQueue.add(action);
        synchronized (actionQueue){
            actionQueue.notify();
        }
    }

    private void processActionQueue(){
        while(!Thread.interrupted()){
            try {
                List<Runnable> pendingAction = new LinkedList<>();
                int availableActions = actionQueue.drainTo(pendingAction);
                if (availableActions > 0) {
                    if(executeOnlyLatest) {
                        pendingAction.get(pendingAction.size() - 1).run();
                    } else {
                        pendingAction.forEach(Runnable::run);
                    }
                } else {
                    synchronized (actionQueue){
                        actionQueue.wait();
                    }
                }
            } catch (InterruptedException e) {
                log.info("Quitting action thread due to interruption", e);
                return;
            } catch(RuntimeException ex){
                log.info("Actions threw exceptions, keeping thread alive: ", ex);
            }
        }
    }
}
