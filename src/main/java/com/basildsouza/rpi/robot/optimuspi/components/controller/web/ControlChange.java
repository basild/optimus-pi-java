package com.basildsouza.rpi.robot.optimuspi.components.controller.web;

import lombok.Data;

@Data
public class ControlChange {
    private String name;
    private int x;
    private int y;
}
