package com.basildsouza.rpi.robot.optimuspi.config;

import com.basildsouza.rpi.robot.optimuspi.components.*;
import com.basildsouza.rpi.robot.optimuspi.components.sensors.LineSensor;
import com.basildsouza.rpi.robot.optimuspi.components.sensors.LineSensorFactory;
import com.basildsouza.rpi.robot.optimuspi.config.pin.LineSensorsPinConfig;
import com.basildsouza.rpi.robot.optimuspi.config.pin.OptimusPiPinConfig;
import com.pi4j.context.Context;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
@RequiredArgsConstructor
public class OptimusPiConfig {
    private final OptimusPiPinConfig pinConfigs;
    private final RobotBodyFactory robotBodyFactory;
    private final ServoMotorFactory servoMotorFactory;
    private final LineSensorFactory lineSensorFactory;

    @Bean
    @ConditionalOnProperty(name = "optimuspi.enable.motors", matchIfMissing = false)
    public RobotBody robotBody(){
        System.out.println(pinConfigs.toString());

        RobotBodyConfig robotBodyConfig
                = RobotBodyConfig.builder().leftMotorForwardPin(pinConfigs.getMotor().getLeft().getForward())
                                           .leftMotorBackwardPin(pinConfigs.getMotor().getLeft().getBackward())
                                           .rightMotorForwardPin(pinConfigs.getMotor().getRight().getForward())
                                           .rightMotorBackwardPin(pinConfigs.getMotor().getRight().getBackward()).build();
        return robotBodyFactory.newRobotBody(robotBodyConfig);
    }

    @Bean
    public ServoMotor panServo(){
        return servoMotorFactory.newMotor("Tilt", pinConfigs.getCamera().getPan());
    }

    @Bean
    public ServoMotor tiltServo(){
        return servoMotorFactory.newMotor("Pan", pinConfigs.getCamera().getTilt());
    }

    @Bean
    public LineSensorBoard lineSensorBoard(){
        Map<String, LineSensor> lineSensors = new HashMap<>();
        LineSensorsPinConfig lineSensorsPinConfig = pinConfigs.getSensor().getLine();
        lineSensors.put("left-wing", lineSensorFactory.newLineSensor("left-wing", lineSensorsPinConfig.getLeftWing()));
        lineSensors.put("left", lineSensorFactory.newLineSensor("left", lineSensorsPinConfig.getLeft()));
        lineSensors.put("centre", lineSensorFactory.newLineSensor("centre", lineSensorsPinConfig.getCentre()));
        lineSensors.put("right", lineSensorFactory.newLineSensor("right", lineSensorsPinConfig.getRight()));
        lineSensors.put("right-wing", lineSensorFactory.newLineSensor("right-wing", lineSensorsPinConfig.getRightWing()));

        return new LineSensorBoard(lineSensors);
    }
}
