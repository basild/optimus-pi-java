package com.basildsouza.rpi.robot.optimuspi.components;

import com.pi4j.io.pwm.Pwm;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Motor {
    private final Pwm forwardPin;
    private final Pwm backwardPin;
    private static final double MAX_SPEED = 1.0;
    private static final double MIN_SPEED = 1.0;

    public void move(double speed){
        if(speed > 0.0){
            forward(speed);
        } else if (speed < 0.0){
            backward(speed * -1);
        } else {
            stop();
        }
    }
    public void forward(){
        forward(MAX_SPEED);
    }

    public void forward(double speed){
        //TODO: Verify range
        forwardPin.on(speed * 100.00);
        backwardPin.off();
    }
    public void backward(){
        backward(MAX_SPEED);
    }

    public void backward(double speed){
        //TODO: Verify range
        backwardPin.on(speed * 100.00);
        forwardPin.off();
    }

    public void stop(){
        forwardPin.off();
        backwardPin.off();
    }
}


