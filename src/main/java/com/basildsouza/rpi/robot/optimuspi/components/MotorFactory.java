package com.basildsouza.rpi.robot.optimuspi.components;

import com.pi4j.context.Context;
import com.pi4j.io.pwm.Pwm;
import com.pi4j.io.pwm.PwmType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class MotorFactory {
    private final Context context;
    private static final PwmType DEFAULT_PWM_TYPE = PwmType.SOFTWARE;
    //private static final String DEFAULT_PROVIDER = "pigpio-pwm";

    public Motor newMotor(String motorId, int forwardPin, int backwardPin) {
        return new Motor(createPin("forward-" + motorId, forwardPin),
                createPin("backward-" + motorId, backwardPin));
    }

    private Pwm createPin(String motorId, int pin) {
        var pwmPinConfig = Pwm.newConfigBuilder(context)
                .id("motor-pin-" + motorId)
                .name("Motor Pin - " + motorId)
                .address(pin)
                .pwmType(DEFAULT_PWM_TYPE)
                .shutdown(0)
                .initial(0)
                //.provider(DEFAULT_PROVIDER)
                .build();

        return context.create(pwmPinConfig);
    }
}
