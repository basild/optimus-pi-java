package com.basildsouza.rpi.robot.optimuspi.components.sensors;

import com.basildsouza.rpi.robot.optimuspi.components.events.SpecificStateListener;
import com.pi4j.context.Context;
import com.pi4j.io.gpio.digital.DigitalInput;
import com.pi4j.io.gpio.digital.DigitalInputConfig;
import com.pi4j.io.gpio.digital.DigitalState;
import com.pi4j.io.gpio.digital.PullResistance;

public class DigitalSensor extends AbstractDigitalSensor<DigitalState> {
    public DigitalSensor(DigitalInput digitalInput) {
        super(digitalInput);
    }

    public boolean isActive(){
        return isActive(false);
    }

    public boolean isActive(boolean refreshState){
        return doesStateMatch(DigitalState.HIGH, refreshState);
    }

    public boolean isInactive(){
        return isInactive(false);
    }

    public boolean isInactive(boolean refreshState){
        return doesStateMatch(DigitalState.LOW, refreshState);
    }

    public boolean doesStateMatch(DigitalState expectedState, boolean refreshState){
        return getState() == expectedState;
    }

    @Override
    public DigitalState mapDigitalState(DigitalState digitalState) {
        return digitalState;
    }

    public void whenActive(SpecificStateListener changeListener){
        super.addSpecificStateListener(changeListener, DigitalState.HIGH);
    }

    public void whenInactive(SpecificStateListener changeListener){
        super.addSpecificStateListener(changeListener, DigitalState.LOW);
    }
}

