package com.basildsouza.rpi.robot.optimuspi.components.sensors;

import com.basildsouza.rpi.robot.optimuspi.components.events.SpecificStateListener;
import com.pi4j.context.Context;
import com.pi4j.io.gpio.digital.DigitalInput;
import com.pi4j.io.gpio.digital.DigitalState;

import java.util.Map;

public class LineSensor extends AbstractDigitalSensor<LineSensor.LineState> {
    private static final Map<DigitalState, LineState> stateMapping =
            Map.of(DigitalState.LOW, LineState.OFF_LINE,
                   DigitalState.HIGH, LineState.ON_LINE);


    public LineSensor(DigitalInput digitalInput) {
        super(digitalInput);
    }

    public boolean isOnLine(){
        return isOnLine(false);
    }

    public boolean isOnLine(boolean refreshState){
        return doesStateMatch(LineState.ON_LINE, refreshState);
    }

    public boolean isOffLine(){
        return isOffLine(false);
    }

    public boolean isOffLine(boolean refreshState){
        return doesStateMatch(LineState.OFF_LINE, refreshState);
    }

    public boolean doesStateMatch(LineState expectedState, boolean refreshState){
        return getState() == expectedState;
    }

    @Override
    public LineState mapDigitalState(DigitalState digitalState) {
        return stateMapping.getOrDefault(digitalState, LineState.OFF_LINE);
    }

    public void whenOnLine(SpecificStateListener changeListener){
        super.addSpecificStateListener(changeListener, LineState.ON_LINE);
    }

    public void whenOffLine(SpecificStateListener changeListener){
        super.addSpecificStateListener(changeListener, LineState.OFF_LINE);
    }

    public enum LineState {
        ON_LINE,
        OFF_LINE
    }
}

