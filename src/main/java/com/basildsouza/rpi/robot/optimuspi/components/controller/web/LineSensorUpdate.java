package com.basildsouza.rpi.robot.optimuspi.components.controller.web;

import com.basildsouza.rpi.robot.optimuspi.components.sensors.LineSensor;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
public class LineSensorUpdate {
    private Date updatedOn;
    private List<LineSensorState> lineSensors;
}

@Data
@AllArgsConstructor
class LineSensorState {
    private String name;
    private LineSensor.LineState state;
}
