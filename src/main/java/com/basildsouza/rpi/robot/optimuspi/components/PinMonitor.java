package com.basildsouza.rpi.robot.optimuspi.components;

import com.pi4j.io.IO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class PinMonitor {
    private final Map<Integer, IO> usedPins = new HashMap<>();

    public void registerUsage(int pinNumber, IO pin){
        log.info("Registered usage of pin number: " + pinNumber + ", ID: " + pin.id() + ", Type: " + pin.type() + ", Name: " + pin.name());
        usedPins.put(pinNumber, pin);
    }

    public <T extends IO> T getPin(int pinNumber, Class<T> pinClass){
        if(!usedPins.containsKey(pinNumber)){
            throw new IllegalStateException("No usage of pin number: " + pinNumber + " is registered with this class");
        }
        IO io = usedPins.get(pinNumber);
        if(!pinClass.equals(io.getClass())){
            throw new IllegalStateException("Pin Number: " + pinNumber + " is of type: " + io.getClass() + "not: " + pinClass);
        }

        return (T) io;
    }
}
