package com.basildsouza.rpi.robot.optimuspi.components.sensors;

import com.basildsouza.rpi.robot.optimuspi.components.events.*;
import com.pi4j.context.Context;
import com.pi4j.io.gpio.digital.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

//@RequiredArgsConstructor
// TODO: Convert to Abstract Digital Sensor and then make a Simple Digital Sensor
public abstract class AbstractDigitalSensor<STATE> { //TODO: Get the "STATE" via an alternate interface?
    @Getter
    private final DigitalInput digitalInput;
    private final List<StateChangeListener<STATE>> registeredEventListeners = new LinkedList<>();
    private final AtomicReference<StateRecord<STATE>> lastRecordedState = new AtomicReference<>();

    public abstract STATE mapDigitalState(DigitalState digitalState);

    public AbstractDigitalSensor(DigitalInput digitalInput) {
        this.digitalInput = digitalInput;
        setState(mapDigitalState(DigitalState.LOW), System.nanoTime());
        digitalInput.addListener(this::onStateChange);
    }

    public STATE getState() {
        return getState(true);
    }

    private void setState(STATE state, long time){
        lastRecordedState.set(new StateRecord<>(state, time));
    }

    public STATE getState(boolean refresh) {
        if(refresh){
            setState(mapDigitalState(digitalInput.state()), System.nanoTime());
        }
        return lastRecordedState.get().getState();
    }

    public StateRecord<STATE> getStateRecord(){
        return lastRecordedState.get();
    }

    private void onStateChange(DigitalStateChangeEvent<DigitalInput> changeEvent){
        STATE state = mapDigitalState(changeEvent.state());
        setState(state, System.nanoTime());
        registeredEventListeners.forEach(listener -> listener.onStateChange(state));
    }

    public void addChangeListener(StateChangeListener<STATE> changeListener){
        registeredEventListeners.add(changeListener);
    }

    protected void addSpecificStateListener(SpecificStateListener changeListener, STATE state){
        addChangeListener(newState -> executeIfMatching(changeListener, newState, state));
    }

    private void executeIfMatching(SpecificStateListener runIfMatches, STATE actualState, STATE expectedState){
        if(actualState.equals(expectedState)){
            runIfMatches.onMatchingState();
        }
    }

    @RequiredArgsConstructor
    @Getter
    public static class StateRecord<STATE> {
        private final STATE state;
        private final long changedAt;

        public boolean matches(STATE state){
            return this.state == state;
        }
    }
/*
    public boolean isNoisy() {
        return this.getState() == SoundState.NOISE;
    }

    public boolean isSilent() {
        return this.getState() == SoundState.SILENT;
    }
*/



//    @Override
//    public STATE mapDigitalState(DigitalState digitalState) {
//        switch (digitalState) {
//            case LOW:
//                return SoundState.NOISE;
//            case HIGH:
//                return SoundState.SILENT;
//            case UNKNOWN:
//            default:
//                return SoundState.UNKNOWN;
//        }
//    }
}

