package com.basildsouza.rpi.robot.optimuspi.components;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
// TODO: This might not be required any more
public class RobotBodyConfig {
    private final int leftMotorForwardPin;
    private final int leftMotorBackwardPin;
    private final int rightMotorForwardPin;
    private final int rightMotorBackwardPin;
}
