package com.basildsouza.rpi.robot.optimuspi.config.pin;

import lombok.Data;

@Data
public class LineSensorsPinConfig {
    private Integer leftWing;
    private Integer left;
    private Integer centre;
    private Integer right;
    private Integer rightWing;
}
