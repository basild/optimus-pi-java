package com.basildsouza.rpi.robot.optimuspi.components;

import com.pi4j.context.Context;
import com.pi4j.io.pwm.Pwm;
import com.pi4j.io.pwm.PwmType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class ServoMotorFactory {
    /**
     * Default PWM frequency of the servo
     */
    public final static int DEFAULT_FREQUENCY = 50;

    private final Context context;
    private static final PwmType DEFAULT_PWM_TYPE = PwmType.SOFTWARE;
    private static final String DEFAULT_PROVIDER = "pigpio-pwm";

    public ServoMotor newMotor(String motorId, int pin, int frequency) {
        return new ServoMotor(createPin(motorId, pin, frequency));
    }

    public ServoMotor newMotor(String motorId, int pin) {
        return newMotor(motorId, pin, DEFAULT_FREQUENCY);
    }

    private Pwm createPin(String motorId, int pin, int frequency) {
        var pwmPinConfig = Pwm.newConfigBuilder(context)
                .id("servo-motor-pin-" + motorId)
                .name("Servo Motor Pin - " + motorId)
                .address(pin)
                .pwmType(DEFAULT_PWM_TYPE)
                .frequency(frequency)
                .shutdown(0)
                .initial(0)
                .provider(DEFAULT_PROVIDER)
                .build();

        return context.create(pwmPinConfig);
    }
}
