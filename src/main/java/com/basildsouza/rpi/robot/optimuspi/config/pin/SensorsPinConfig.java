package com.basildsouza.rpi.robot.optimuspi.config.pin;

import lombok.Data;

@Data
public class SensorsPinConfig {
    private LineSensorsPinConfig line;
}
