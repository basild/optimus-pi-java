package com.basildsouza.rpi.robot.optimuspi.config.pin;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "optimuspi.pins")
@Data
public class OptimusPiPinConfig {
    private MotorsPinConfig motor;
    private CameraPinConfig camera;
    private SensorsPinConfig sensor;
}

