package com.basildsouza.rpi.robot.optimuspi.components;

import com.pi4j.io.pwm.Pwm;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Implementation of a generic Servo Motor
 */
@RequiredArgsConstructor
@Getter
public class ServoMotor {
    /**
     * Default minimum angle of the servo motor
     */
    public final static double DEFAULT_MIN_ANGLE = -90;
    /**
     * Default maximum angle of the servo motor
     */
    public static final double DEFAULT_MAX_ANGLE = 90;

    /**
     * Default minimum PWM duty cycle to put the PWM into the minimum angle position
     */
    public final static double DEFAULT_MIN_DUTY_CYCLE = 2;
    /**
     * Maximum PWM duty cycle to put the PWM into the maximum angle position
     */
    public final static double DEFAULT_MAX_DUTY_CYCLE = 12;

    /**
     * Pi4J PWM instance for this servo
     */
    private final Pwm motorPin;

    /**
     * Minimum angle of the servo motor used for this instance, should match previously tested real world values
     */
    private final double minAngle;
    /**
     * Maximum angle of the servo motor used for this instance, should match previously tested real world values
     */
    private final double maxAngle;
    /**
     * Minimum duty cycle of the servo motor for this instance, should match previously tested real world values
     */
    private final double minDutyCycle;
    /**
     * Maximum duty cycle of the servo motor for this instance, should match previously tested real world values
     */
    private final double maxDutyCycle;

    /**
     * Minimum value for user-defined range, defaults to 0
     */
    private double minRange = 0;
    /**
     * Maximum value for user-defined range, defaults to 1
     */
    private double maxRange = 1;

    private double currentPosition = 0;

    /**
     * Builds a Service with the default settings for the the angles and duty cycle
     */
    public ServoMotor(Pwm motorPin){
        this(motorPin, DEFAULT_MIN_ANGLE, DEFAULT_MAX_ANGLE, DEFAULT_MIN_DUTY_CYCLE, DEFAULT_MAX_DUTY_CYCLE);
    }

    /**
     * Rotates the servo motor to the specified angle in degrees.
     * The angle should be between  {@link #getMinAngle()} and {@link #getMaxAngle()} which was specified during initialization.
     * Values outside of this inclusive range are automatically being clamped to their respective minimum / maximum.
     *
     * @param angle New absolute angle
     */
    public void setAngle(double angle) {
        motorPin.on(mapAngleToDutyCycle(angle));
    }

    /**
     * Rotates the servo by mapping a percentage value to the range between {@link #getMinAngle()} and {@link #getMaxAngle()}.
     * As an example, a value of 0% will equal to the minimum angle, 50% to the center and 100% to the maximum angle.
     *
     * @param percent Percentage value, automatically clamped between 0 and 100
     */
    public void setPercent(double percent) {
        moveOnRange(percent, 0, 100);
    }

    /**
     * Maps the given value based on the range previously defined with {@link #setRange(double, double)} to the full range of the servo.
     * If {@link #setRange(double, double)} was not called before, the default range of 0-1 (as double) is being used.
     *
     * @param value Value to map
     */
    public void moveOnRange(double value) {
        moveOnRange(value, minRange, maxRange);
    }

    /**
     * Maps the given value based on the given input range to the full range of the servo.
     * Unlike {@link #moveOnRange(double)}, this method will NOT use or adjust the values set by {@link #setRange(double, double)}.
     *
     * @param value Value to map
     * @param minValue Minimum range value
     * @param maxValue Maximum range value
     */
    public void moveOnRange(double value, double minValue, double maxValue) {
        motorPin.on(mapToDutyCycle(value, minValue, maxValue));
    }

    /**
     * Adjusts the minimum and maximum for the user-defined range which can be used in combination with {@link #moveOnRange(double)}.
     * This method will only affect future calls to {@link #moveOnRange(double)} and does not change the current position.
     *
     * @param minValue Minimum range value
     * @param maxValue Maximum range value
     */
    public void setRange(double minValue, double maxValue) {
        this.minRange = minValue;
        this.maxRange = maxValue;
    }

    /**
     * Returns the minimum angle configured for this servo.
     *
     * @return Minimum angle in degrees
     */
    public double getMinAngle() {
        return minAngle;
    }

    /**
     * Returns the maximum angle configured for this servo.
     *
     * @return Maximum angle in degrees
     */
    public double getMaxAngle() {
        return maxAngle;
    }

    /**
     * Helper function to map an angle between {@link #minAngle} and {@link #maxAngle} to the configured duty cycle range.
     *
     * @param angle Desired angle
     * @return Duty cycle required to achieve this position
     */
    private double mapAngleToDutyCycle(double angle) {
        return mapToDutyCycle(angle, minAngle, maxAngle);
    }

    /**
     * Helper function to map an input value between a specified range to the configured duty cycle range.
     *
     * @param input      Value to map
     * @param inputStart Minimum value for custom range
     * @param inputEnd   Maximum value for custom range
     * @return Duty cycle required to achieve this position
     */
    private double mapToDutyCycle(double input, double inputStart, double inputEnd) {
        return mapRange(input, inputStart, inputEnd, minDutyCycle, maxDutyCycle);
    }

    /**
     * Helper function to map an input value from its input range to a possibly different output range.
     *
     * @param input       Input value to map
     * @param inputStart  Minimum value for input
     * @param inputEnd    Maximum value for input
     * @param outputStart Minimum value for output
     * @param outputEnd   Maximum value for output
     * @return Mapped input value
     */
    private static double mapRange(double input, double inputStart, double inputEnd, double outputStart, double outputEnd) {
        // Automatically swap minimum/maximum of input if inverted
        if (inputStart > inputEnd) {
            final double tmp = inputEnd;
            inputEnd = inputStart;
            inputStart = tmp;
        }

        // Automatically swap minimum/maximum of output if inverted
        if (outputStart > outputEnd) {
            final double tmp = outputEnd;
            outputEnd = outputStart;
            outputStart = tmp;
        }

        // Automatically clamp the input value and calculate the mapped value
        final double clampedInput = Math.min(inputEnd, Math.max(inputStart, input));
        return outputStart + ((outputEnd - outputStart) / (inputEnd - inputStart)) * (clampedInput - inputStart);
    }
}


