package com.basildsouza.rpi.robot.optimuspi.config.pin;

import lombok.Data;

@Data
public class MotorsPinConfig {
    private MotorPinConfig left;
    private MotorPinConfig right;
}
