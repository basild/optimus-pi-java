package com.basildsouza.rpi.robot.optimuspi.components;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class RobotBodyFactory {
    private final MotorFactory motorFactory;

    public RobotBody newRobotBody(RobotBodyConfig robotBodyConfig) {
        return new RobotBody(motorFactory.newMotor("left", robotBodyConfig.getLeftMotorForwardPin(),
                robotBodyConfig.getLeftMotorBackwardPin()),
                motorFactory.newMotor("right", robotBodyConfig.getRightMotorForwardPin(),
                        robotBodyConfig.getRightMotorBackwardPin()));
    }
}
