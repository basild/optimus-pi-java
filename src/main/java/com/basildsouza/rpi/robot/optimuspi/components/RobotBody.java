package com.basildsouza.rpi.robot.optimuspi.components;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RobotBody {
    private final Motor leftMotor;
    private final Motor rightMotor;

    public enum Direction{
        STOP,
        FORWARD,
        BACKWARD,
        COUNTER_CLOCKWISE,
        CLOCKWISE,
        FORWARD_LEFT,
        FORWARD_RIGHT,
        BACKWARD_LEFT,
        BACKWARD_RIGHT,
        ANALOG_CONTROL
    };

    private Direction direction;

    public void move(double leftSpeed, double rightSpeed){
        direction = Direction.ANALOG_CONTROL;
        leftMotor.move(leftSpeed);
        rightMotor.move(rightSpeed);
        System.out.println("Robot - Analog Control");
    }

    public void forward(double speed) {
        if (alreadyInState(Direction.FORWARD)) return;
        leftMotor.forward(speed);
        rightMotor.forward(speed);
        System.out.println("Robot - Forward");
    }

    public void backward(double speed) {
        if (alreadyInState(Direction.BACKWARD)) return;
        leftMotor.backward(speed);
        rightMotor.backward(speed);
    }

    public void stop() {
        if (alreadyInState(Direction.STOP)) return;
        leftMotor.stop();
        rightMotor.stop();
    }

    public void spinCounterClockwise(double speed) {
        if (alreadyInState(Direction.COUNTER_CLOCKWISE)) return;
        leftMotor.backward(speed);
        rightMotor.forward(speed);
    }

    public void spinClockwise(double speed) {
        if (alreadyInState(Direction.CLOCKWISE)) return;

        leftMotor.forward(speed);
        rightMotor.backward(speed);
    }

    public void forwardLeft(double speed){
        if (alreadyInState(Direction.FORWARD_LEFT)) return;

        leftMotor.stop();
        rightMotor.forward(speed);
    }

    public void forwardRight(double speed){
        if (alreadyInState(Direction.FORWARD_RIGHT)) return;
        leftMotor.forward(speed);
        rightMotor.stop();
    }

    public void backwardLeft(double speed){
        if (alreadyInState(Direction.BACKWARD_LEFT)) return;

        leftMotor.stop();
        rightMotor.backward(speed);
    }

    public void backwardRight(double speed){
        if (alreadyInState(Direction.BACKWARD_RIGHT)) return;
        leftMotor.backward(speed);
        rightMotor.stop();
    }

    private boolean alreadyInState(Direction newDirection) {
        if (direction == newDirection) {
            return true;
        }
        System.out.println("Robot - " + newDirection);
        direction = newDirection;
        return false;
    }

}

