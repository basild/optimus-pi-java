package com.basildsouza.rpi.robot.optimuspi.config.pin;

import lombok.Data;

@Data
public class MotorPinConfig {
    private int forward;
    private int backward;
}
