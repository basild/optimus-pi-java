package com.basildsouza.rpi.robot.optimuspi.config.pin;

import lombok.Data;

@Data
public class CameraPinConfig {
    private int pan;
    private int tilt;
}
