package com.basildsouza.rpi.robot.optimuspi.components.sensors;

import com.pi4j.context.Context;
import com.pi4j.io.gpio.digital.DigitalInput;
import org.springframework.stereotype.Component;

@Component
public class LineSensorFactory extends AbstractDigitalSensorFactory {
    private static final int DEFAULT_DEBOUNCE = 500;
    private static final String NAME = "LineSensor";

    public LineSensorFactory(Context context) {
        super(context, NAME, DEFAULT_DEBOUNCE);
    }

    public LineSensor newLineSensor(String position, int pin) {
        return newLineSensor(position, pin, DEFAULT_DEBOUNCE);
    }

    public LineSensor newLineSensor(String position, int pin, int debounce) {
        return new LineSensor(buildDigitalInput(position, pin, debounce));
    }
}
