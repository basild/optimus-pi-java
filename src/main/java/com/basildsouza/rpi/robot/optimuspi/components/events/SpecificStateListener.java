package com.basildsouza.rpi.robot.optimuspi.components.events;

@FunctionalInterface
public interface SpecificStateListener {
    void onMatchingState();
}