package com.basildsouza.rpi.robot.optimuspi.components.controller.web;

import com.basildsouza.rpi.robot.optimuspi.components.LineSensorBoard;
import com.basildsouza.rpi.robot.optimuspi.components.controller.AnalogRobotController;
import com.basildsouza.rpi.robot.optimuspi.components.controller.CameraController;
import com.basildsouza.rpi.robot.optimuspi.components.controller.QueueActionExecutor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import java.util.Date;
import java.util.List;

@Controller
@Slf4j
@RequiredArgsConstructor
public class RobotWebController {
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final AnalogRobotController analogRobotController;
    private final CameraController cameraController;
    private final LineSensorBoard lineSensorBoard;

    private final QueueActionExecutor moveActionExecutor = new QueueActionExecutor("Move-Action", false);
    private final QueueActionExecutor cameraActionExecutor = new QueueActionExecutor("Camera-Action", true);

    @MessageMapping("/control/camera")
    public void cameraControlChanged(ControlChange controlChange) throws Exception {
        log.info("Camera Control Change Event Received: " + controlChange);
        cameraActionExecutor.registerAction(() -> cameraController.moveCamera(controlChange.getX(), controlChange.getY()));
    }

    @MessageMapping("/control/move")
    public void moveControlChanged(ControlChange controlChange) throws Exception {
        log.info("Move Control Change Event Received: " + controlChange);
        moveActionExecutor.registerAction(() -> analogRobotController.moveRobot(controlChange.getX(), controlChange.getY()));
    }

    /* TODO: Add more control items:
        * Enable Modes: Line following / etc as and when implemented
        * What else?
     */


    @Scheduled(fixedDelay = 1000) //TODO: Change frequency to 1 sec or less
    public void sendLineSensorUpdate() {
        log.info("Sending Line sensor update");
        simpMessagingTemplate.convertAndSend("/topic/sensor/line",
                new LineSensorUpdate(new Date(), List.of(
                    getLineSensorState("left-wing"),
                    getLineSensorState("left"),
                    getLineSensorState("centre"),
                    getLineSensorState("right"),
                    getLineSensorState("right-wing"))));
    }

    private LineSensorState getLineSensorState(String position){
        return new LineSensorState(position, lineSensorBoard.getSensorFromPosition(position).getState());
    }

    @Scheduled(fixedRate = 10000) //TODO: Change frequency to 1 sec or less
    public void sendDistanceSensorUpdate() {
        log.info("Sending Distance sensor update");
        simpMessagingTemplate.convertAndSend("/topic/sensor/distance",
                new DistanceSensorUpdate(new Date(), 60, 50.0));
    }

}
