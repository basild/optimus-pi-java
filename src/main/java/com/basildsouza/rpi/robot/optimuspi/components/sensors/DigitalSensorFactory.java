package com.basildsouza.rpi.robot.optimuspi.components.sensors;

import com.pi4j.context.Context;
import org.springframework.stereotype.Component;

@Component
public class DigitalSensorFactory extends AbstractDigitalSensorFactory {
    private static final int DEFAULT_DEBOUNCE = 1000;
    private static final String NAME = "DigitalSensor";

    public DigitalSensorFactory(Context context) {
        super(context, NAME, DEFAULT_DEBOUNCE);
    }

    public DigitalSensor newDigitalSensor(String name, int pin, int debounce) {
        return new DigitalSensor(buildDigitalInput(name, pin, debounce));
    }
}
