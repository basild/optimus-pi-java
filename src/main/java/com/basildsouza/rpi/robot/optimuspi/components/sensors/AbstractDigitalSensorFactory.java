package com.basildsouza.rpi.robot.optimuspi.components.sensors;

import com.pi4j.context.Context;
import com.pi4j.io.gpio.digital.DigitalInput;
import com.pi4j.io.gpio.digital.PullResistance;
import org.springframework.stereotype.Component;

public class AbstractDigitalSensorFactory {
    private final Context context;
    private final String sensorName;
    private final int defaultDebounce;
    private final PullResistance defaultPullResistance;

    // private final static String DEFAULT_PROVIDER = "pigpio-digital-input";

    public AbstractDigitalSensorFactory(Context context, String sensorName, int defaultDebounce, PullResistance defaultPullResistance) {
        this.context = context;
        this.sensorName = sensorName;
        this.defaultDebounce = defaultDebounce;
        this.defaultPullResistance = defaultPullResistance;
    }

    public AbstractDigitalSensorFactory(Context context, String sensorName, int defaultDebounce) {
        this(context, sensorName, defaultDebounce, PullResistance.PULL_UP);
    }

    protected DigitalInput buildDigitalInput(String name, int address, long debounce) {
        return context.create(DigitalInput.newConfigBuilder(context)
                .id("BCM" + address)
                .name(sensorName + " - " + name)
                .address(address)
                .pull(defaultPullResistance)
                .debounce(debounce)
                //.provider(DEFAULT_PROVIDER)
                .build());
    }
}
