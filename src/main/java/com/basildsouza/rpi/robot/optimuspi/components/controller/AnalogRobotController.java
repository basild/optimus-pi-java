package com.basildsouza.rpi.robot.optimuspi.components.controller;

import com.basildsouza.rpi.robot.optimuspi.components.RobotBody;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@AllArgsConstructor
public class AnalogRobotController {
    private RobotBody robotBody;

    private static final double MaxJoy = 100;
    private static final double MinJoy = -100;
    private static final double MaxValue = 100;
    private static final double MinValue = -100;

    public void moveRobot(int x, int y){
        double hypo = Math.sqrt(x * x + y * y);
        double radians = Math.acos(Math.abs(x) / hypo);

        if(Double.isNaN(radians)){
            radians = 0.0;
        }

        double angle = radians * 180 / Math.PI;

        // Now angle indicates the measure of turn
        // Along a straight line, with an angle o, the turn co-efficient is same
        // this applies for angles between 0-90, with angle 0 the co-eff is -1
        // with angle 45, the co-efficient is 0 and with angle 90, it is 1
        double tcoeff = -1 + (angle / 90) * 2;
        double turn = tcoeff * Math.abs(Math.abs(y) - Math.abs(x));

        turn = Math.round(turn * 100) / 100.00;
        // And max of y or x is the movement
        double move = Math.max(Math.abs(y), Math.abs(x));

        double rawLeft;
        double rawRight;
        // First and third quadrant
        if ((x >= 0 && y >= 0) || (x < 0 && y < 0)) {
            rawLeft = move;
            rawRight = turn;
        } else {
            rawRight = move;
            rawLeft = turn;
        }
        // Reverse polarity
        if (y < 0) {
            rawLeft = 0 - rawLeft;
            rawRight = 0 - rawRight;
        }

        robotBody.move(remap(rawLeft, MinJoy, MaxJoy, MinValue, MaxValue),
                       remap(rawRight, MinJoy, MaxJoy, MinValue, MaxValue));
    }

    private double remap(double value, double from1, double to1, double from2, double to2) {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}
