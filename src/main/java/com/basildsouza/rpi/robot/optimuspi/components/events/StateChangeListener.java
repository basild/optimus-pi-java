package com.basildsouza.rpi.robot.optimuspi.components.events;

@FunctionalInterface
public interface StateChangeListener<STATE> {
    void onStateChange(STATE newState);
}