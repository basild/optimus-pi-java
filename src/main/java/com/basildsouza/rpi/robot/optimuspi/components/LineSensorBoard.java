package com.basildsouza.rpi.robot.optimuspi.components;

import com.basildsouza.rpi.robot.optimuspi.components.sensors.LineSensor;
import lombok.RequiredArgsConstructor;

import java.util.Map;

@RequiredArgsConstructor
public class LineSensorBoard {
    private final Map<String, LineSensor> lineSensors;

    public LineSensor getSensorFromPosition(String position){
        return lineSensors.get(position);
    }
}

