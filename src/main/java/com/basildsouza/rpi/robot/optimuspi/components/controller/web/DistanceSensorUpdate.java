package com.basildsouza.rpi.robot.optimuspi.components.controller.web;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
public class DistanceSensorUpdate {
    private Date updatedOn;
    private int normalizedDistance;
    private double actualDistanceCm;
}