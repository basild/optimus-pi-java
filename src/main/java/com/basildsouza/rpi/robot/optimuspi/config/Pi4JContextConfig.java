package com.basildsouza.rpi.robot.optimuspi.config;

import com.pi4j.Pi4J;
import com.pi4j.context.Context;
import com.pi4j.platform.Platforms;
import com.pi4j.util.Console;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PreDestroy;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class Pi4JContextConfig {
    private static final Console console = new Console();
    private Context context;

    @Value("${optimuspi.remote.enable}")
    private Boolean remote;

    @Value("${optimuspi.remote.host}")
    private String host;

    public Pi4JContextConfig(){
        //TODO: Can pass in properties here or as members
    }

    @Bean
    public Context context(){
        console.box("Optimus Pi - Booting Up");
        Map<String, String> pigioProperties = new HashMap<>();
        // TODO: Pass these in as properties
        pigioProperties.put("remote", System.getProperty("REMOTE_ROBOT", remote.toString()));
        pigioProperties.put("host", host);

        context = Pi4J.newContextBuilder()
                //.defaultPlatform("mock-platform")
                .autoDetect().properties(pigioProperties).build();

        Platforms platforms = context.platforms();

        console.box("Pi4J PLATFORMS");
        console.println();
        platforms.describe().print(System.out);
        console.println();

        return context;
    }

    @PreDestroy
    public void shutdownContext(){
        if(context != null){
            context.shutdown();
        }
    }
}
