package com.basildsouza.rpi.robot.optimuspi.components.controller;

import com.basildsouza.rpi.robot.optimuspi.components.ServoMotor;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@Slf4j
public class CameraController {
    private final ServoMotor panServo;
    private final ServoMotor tiltServo;

    public void moveCamera(int pan, int tilt){
        pan(pan, -100, 100);
        tilt(tilt, -100, 100);
    }

    public void pan(int range, int minValue, int maxValue){
        panServo.moveOnRange(range, minValue, maxValue);
    }

    public void tilt(int range, int minValue, int maxValue){
        tiltServo.moveOnRange(range, minValue, maxValue);
    }
}
