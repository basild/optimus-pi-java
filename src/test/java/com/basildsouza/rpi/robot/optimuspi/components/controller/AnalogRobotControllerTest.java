package com.basildsouza.rpi.robot.optimuspi.components.controller;

import com.basildsouza.rpi.robot.optimuspi.components.RobotBody;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

class AnalogRobotControllerTest {
    private RobotBody mockRobotBody = Mockito.mock(RobotBody.class);
    private AnalogRobotController systemUnderTest = new AnalogRobotController(mockRobotBody);

    @Test
    public void testStop(){
        verifyMovement(0, 0, 0, 0);
    }

    @Test
    public void testStraightForward(){
        verifyMovement(0, 100, 100, 100);
        verifyMovement(0, 50, 50, 50);
        verifyMovement(0, 25, 25, 25);
    }

    @Test
    public void testStraightBackwards(){
        verifyMovement(0, -100, -100, -100);
        verifyMovement(0, -50, -50, -50);
        verifyMovement(0, -25, -25, -25);
    }

    @Test
    public void testSpinLeft(){
        verifyMovement(-100, 0, -100, 100);
        verifyMovement(-50, 0, -50, 50);
        verifyMovement(-25, 0, -25, 25);
    }

    @Test
    public void testSpinRight(){
        verifyMovement(100, 0, 100, -100);
        verifyMovement(50, 0, 50, -50);
        verifyMovement(25, 0, 25, -25);
    }

    @Test
    public void testForwardLeft(){
        verifyMovement(-100, 100, 0, 100);
    }

    @Test
    public void testForwardRight(){
        verifyMovement(100, 100, 100, 0);
    }

    @Test
    public void testBackwardRight(){
        verifyMovement(-100, -100, -100, 0);
    }

    @Test
    public void testBackwardLeft(){
        verifyMovement(100, -100, 0, -100);
    }

    private void verifyMovement(int inputX, int inputY,
                                double expectedLeft, double expectedRight){
        systemUnderTest.moveRobot(inputX, inputY);
        Mockito.verify(mockRobotBody).move(expectedLeft, expectedRight);
    }
}